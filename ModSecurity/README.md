ModSecurity installation on Debian 10
-------------------------------------

Before starting install nginx through `apt-get install nginx` afterwards check your nginx version using `nginx -v`, after doing that edit the build to reflect that.
In a future update this will be done automatically.

1. ``cd /opt/``
2. Git Clone this repository in /opt/
3. ``chmod +x /opt/debian-build-tools/modsecurity/build_nginx_modsecurity-debian.sh && /opt/debian-build-tools/modsecurity/build_nginx_modsecurity-debian.sh``
4. Once installation is completed ``nano /etc/nginx/nginx.conf`` and add ``load_module modules/ngx_http_modsecurity_module.so;`` at the very top of the page
5. To enable mod security on the website ``nano /etc/nginx/sites-available/default`` and add 

```
    server {
        modsecurity on;
        modsecurity_rules_file /etc/nginx/modsec/main.conf;
    }
```

if you have the website on a different sites-available file, then add it there instead

6. ``curl localhost?testparam=test`` to confirm that it is working
